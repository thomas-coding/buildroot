################################################################################
#
# userspace_test
#
################################################################################
CDIF_LICENSE = GPL-2.0+

define USERSPACE_TEST_EXTRACT_CMDS
	cp package/userspace_test/userspace_test.c package/userspace_test/userspace_test.h $(@D)/
endef

define USERSPACE_TEST_BUILD_CMDS
	(cd $(@D); $(TARGET_CC) -Wall -Os -s userspace_test.c -o userspace_test)
endef

define USERSPACE_TEST_INSTALL_TARGET_CMDS
	install -m 0755 -D $(@D)/userspace_test $(TARGET_DIR)/usr/bin/userspace_test
endef

$(eval $(generic-package))
